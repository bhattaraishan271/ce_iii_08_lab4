import unittest
from greedy import ActivitySelection,ActivitySelectionRecursive, ActivitySelectionRecursiveSF,sortEvents

class ActivityTest(unittest.TestCase):
	def test_selection(self):
		events = [[1,4],[3,5],[0,6],[5,7],[3,9],[5,9],[6,10],[8,11],[8,12],[2,14],[12,16]]
		selected = [[1,4], [5,7], [8,11], [12,16]]
		self.assertListEqual(ActivitySelection(events,0),selected) #compare lists  ( returned from function, expected value from calculation )

	def test_selection_recursive(self):
		events = [[1,4],[3,5],[0,6],[5,7],[3,9],[5,9],[6,10],[8,11],[8,12],[2,14],[12,16]]
		selected = [[1,4], [5,7], [8,11], [12,16]]
		self.assertListEqual(ActivitySelectionRecursive(events,0,0,len(events)),selected)

	def test_selection_recursive_sf(self):
		events = [[1,4],[3,5],[0,6],[5,7],[3,9],[5,9],[6,10],[8,11],[8,12],[2,14],[12,16]]
		s,f = zip(*events)
		s = list(s)
		f = list(f)
		selected = [[1,4], [5,7], [8,11], [12,16]]

		self.assertListEqual(ActivitySelectionRecursiveSF(events,s, f ,-1),selected)

if __name__=="__main__":
	unittest.main()