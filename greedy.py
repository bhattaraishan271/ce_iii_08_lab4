arr= []
indexArr=[]
arrRecur = []
i = 0
events = [[12,16],[2,14],[8,12],[8,11],[6,10],[5,9],[3,9],[5,7],[0,6],[3,5],[1,4]]
s = []
f = []

def sortEvents():   #sort the events by insertion sort
 	for i in range(1,len(events)):
 		key = events[i]
 		j=i-1
 		while(j>=0 and key[1]<events[j][1]):
 			events[j+1]=events[j]
 			j-=1
 		events[j+1] = key

def ActivitySelection(testEvent,x):
	arr.append(testEvent[x])
	currEvent = arr[len(arr)-1]
	for k in range(1,len(testEvent)):
		if(currEvent[1]<=testEvent[k][0]):
			arr.append(testEvent[k])
			currEvent = arr[len(arr)-1]   #similar to currEvent+=1
	return(arr)


def ActivitySelectionRecursiveSF(testEvent, s,f,k):
	n = len(s)
	m = k + 1
	while m < n and s[m] < f[k] and k >= 0:
		m = m + 1
	if m < n:
		indexArr.append(testEvent[m])
		return ActivitySelectionRecursiveSF(testEvent, s,f,m)
	else:
		return indexArr

def ActivitySelectionRecursive(testEvent,startIndex,currIndex,totalLen):
	if(currIndex<totalLen):
		if(startIndex == currIndex):      #only for first time
			arrRecur.append(testEvent[currIndex])
			currIndex+=1
			return ActivitySelectionRecursive(testEvent,startIndex,currIndex,totalLen)
		else:								#for the successive times
			tempIndex =len(arrRecur)-1
			if(arrRecur[tempIndex][1]<=testEvent[currIndex][0]):
				arrRecur.append(testEvent[currIndex])
			currIndex+=1
			return ActivitySelectionRecursive(testEvent,startIndex,currIndex,totalLen)
	else:
		return arrRecur



if __name__=="__main__":
	sortEvents()
	print(events)

	s,f = zip(*events)
	s = list(s)
	f = list(f)
	#x=input("From the above events select index for which to find successive feasible events:")
	x=0
	print("From Iterative Model")
	print(ActivitySelection(events,x))
	print("From Recursive Model")
	print(ActivitySelectionRecursive(events, x, x ,len(events)))
	print("From Recursive Model with s and f")
	print(ActivitySelectionRecursiveSF(events, s,f,-1))
